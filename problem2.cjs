const fs = require("fs");
function callback_async_fs(){
fs.readFile("../lipsum_1.txt", (err, data) => {
  if (err) {
    console.log(err);
  } else {
    let upperCaseContent = data.toString().toUpperCase();
    fs.writeFile("upperCaseContent.txt", upperCaseContent, (err, data) => {
      if (err) {
        console.log(err);
      } else {
        console.log(data);
        fs.appendFile("fileNames.txt", "upperCaseContent.txt ", (err) => {
          if (err) {
            console.log(err);
          } else {
            fs.readFile("upperCaseContent.txt", (err, data) => {
              if (err) {
                console.log(err);
              } else {
                let lowerCase = data.toString().toLowerCase().split(". ");

                fs.writeFile(
                  "splitedFile.txt",
                  JSON.stringify(lowerCase),
                  (err) => {
                    if (err) {
                      console.log(err);
                    } else {
                      fs.readFile("splitedFile.txt", (err, data) => {
                        if (err) {
                          console.log(err);
                        } else {
                          let newFile = data.sort((a, b) => b - a);
                          fs.writeFile(
                            "newFile.txt",
                            JSON.stringify(newFile),
                            (err) => {
                              if (err) {
                                console.log(err);
                              } else {
                                fs.appendFile(
                                  "fileNames.txt",
                                  "splitedFile.txt newFile.txt ",
                                  (err) => {
                                    if (err) {
                                      console.log(err);
                                    } else {
                                      fs.readFile(
                                        "fileNames.txt",
                                        (err, data) => {
                                          if (err) {
                                            console.log(err);
                                          } else {
                                            fs.readFile(
                                              "fileNames.txt",
                                              (err, data) => {
                                                if (err) {
                                                  console.log(err);
                                                } else {
                                                  data
                                                    .toString()
                                                    .trim()
                                                    .split(" ")
                                                    .forEach((element) => {
                                                      fs.unlink(
                                                        element,
                                                        (err) => {
                                                          if (err) {
                                                            console.log(err);
                                                          } else {
                                                            console.log(
                                                              "sucess"
                                                            );
                                                          }
                                                        }
                                                      );
                                                    });
                                                }
                                              }
                                            );
                                          }
                                        }
                                      );
                                    }
                                  }
                                );
                              }
                            }
                          );
                        }
                      });
                    }
                  }
                );
              }
            });
          }
        });
      }
    });
  }
});
}
// callback_async_fs()
module.exports=callback_async_fs
/*
Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
