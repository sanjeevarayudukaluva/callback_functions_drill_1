const fs = require("fs");
function creating_deleting_fs(){
fs.mkdir("callback", () => console.log("created directory"));
fs.writeFile(
  "callback/problem1.json",
  '{value:"created json file"}',
  (error) => {
    console.log(error);
    console.log("created file");
  }
);

fs.unlink("callback/problem1.json", (err) => {
  console.log(err);
  console.log("file deleted");
});

fs.rmdir("callback", (err) => {
  console.log(err);
  console.log("deleted directory");
});
console.log('sucess')
}
console.log(creating_deleting_fs())
module.exports={creating_deleting_fs}